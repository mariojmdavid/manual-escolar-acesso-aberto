[![License: CC BY 4.0](https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/)

# [Proposta para manuais escolares em acesso aberto](docs/proposta.md)

A proposta encontra-se na directoria
**docs**:

* [Proposta](docs/proposta.md)
* Documento google para comentários de forma mais
fácil: **https://bit.ly/me_acesso_aberto**

## Contribuições

* Sofia Andringa
* Filipe Martins
* Graça Nazaré
