![alt text](88x31.png)
# Proposta para manuais escolares em acesso aberto

### Mário David

(Versão: 0.4.1)

Documento google para comentários de forma mais fácil:

**https://bit.ly/me_acesso_aberto**

Comentários, adicionar texto, discussões são seguidas nos "issues":

**https://gitlab.com/mariojmdavid/manual-escolar-acesso-aberto/issues**

## 1 Objectivo

Esta proposta tem como objectivo providenciar os manuais escolares em
acesso aberto, a todos os alunos de todos os ciclos do ensino obrigatório -
1º ao 12º ano de escolaridade.

Esta proposta aplica-se **apenas a novos manuais** pois, evidentemente, os actuais
estão sujeitos aos direitos de autor e editoras que os produziram e comercializaram.

Os autores são um dos principais visados nesta proposta, dado que são eles
que produzem os manuais escolares. Os outros principais visados são os docentes
e direcções de agrupamentos escolares que fazem a escolha dos manuais escolares.

## 2 Introdução

O acesso aberto de documentos, artigos, livros, tem sido um tema de
discussão crescente. Alguns dos argumentos serão explicados neste documento
como forma de defesa para a produção e disponibilização de manuais escolares em
acesso aberto.

A presente proposta tem como prioridade o interesse de alunos, pais e
professores.

Baseia-se no facto de que uma grande parte dos manuais não são reutilizáveis
após a compra inicial, seja por mudança de um ano para o outro, mesmo dentro da
mesma escola ou grupo escolar, seja por incentivo para que se escreva no
próprio livro.

A disponibilização de manuais a alunos que não tenham dispositivos como
telemóveis, tablets ou computadores, ou acesso
à internet pode ser disponibilizada pelas escolas em forma de cópia em papel. 

## 3 Motivações e Roteiro

O seguinte "Roteiro" é baseado no Decreto-Lei [Ref. 8]:

1. A iniciativa da elaboração e da produção do manual escolar pertence aos autores
2. O manual escolar é submetido a avaliação e certificação, a cargo de comissões
de avaliação, que se traduz na atribuição de uma certificação de qualidade
científico-pedagógica: Custo associado directo.
3. Após avaliação e certificação do manual (ponto anterior), o manual é
directamente disponibilizado numa plataforma digital (site Web), gerido e/ou
controlado a nível do Ministério da Educação, ou outra entidade certificada para
o efeito.
4. As direcções dos agrupamentos escolares e todos os docentes têm acesso livre
(tal como o público em geral), a esta plataforma de modo a poder fazer a
escolha dos manuais escolares.

## 4 Definições

A disponibilização de documentos - **manuais escolares** - em acesso
aberto deverá satisfazer às seguintes condições:

1. É possível encontrar **facilmente** na Internet.
2. É possível aceder-lhes **facilmente**, de forma eletrónica,
(através de telemóveis, tablets, computadores), descarregá-los e imprimí-los.
3. Deve estar num formato que seja **fácil** para qualquer pessoa o poder
ler ou imprimir.
4. Devem ser disponibilizados de **forma gratuita**, embora possa existir uma
forma de contribuição monetária para os autores, se assim o entenderem.
5. Ter um identificador único como por exemplo o ISBN (International
Standard Book Number) ou o DOI (Digital Object Identifier).
6. Ter associado uma licença, como por exemplo da Creative Commons (CC)
[Ref. 1 e 6]:
   * https://creativecommons.org/about/program-areas/open-access/
7. Os manuais devem ter uma versão e tanto quanto possível ser geridos
por um sistema que permita o versionamento de forma fácil: versão no
caso de manuais escolares pode corresponder a número de edição.
8. A autoria dos manuais deve ser clara permitindo a sua referência correcta,
e o devido reconhecimento.
9. Deve ser possível fazer contribuições para o manual, gerir pedidos de
correcção e sugestões para melhorias.
10. A gestão do manual e dos pedidos referidos no ponto anterior,
é feita e aprovada pelos autores.
11. A publicação em acesso aberto permite potencialmente uma grande 
visibilidade e consequentemente um maior escrutínio quando comparado com a
situação actual.

## 5 Enquadramento legal: Certificação, avaliação, acreditação

Os manuais escolares em acesso aberto **devem** passar pelo mesmo processo
de avaliação e certificação que os manuais escolares clássicos. **Só assim faz
sentido esta proposta**.

A [Ref. 7] da *Direção-Geral da Educação, Avaliação e Certificação de Manuais
Escolares 2017-2018*, contém o enquadramento legal referente ao ano lectivo
transacto.

*Sem ter um conhecimento exacto da lei*, o ponto 2 da [Ref. 7], diz:

* 2 – Quem pode submeter manuais escolares à avaliação e certificação?
* Os autores, os editores ou outras instituições legalmente habilitadas
para o efeito, de acordo com o n.º 1 do artigo 5.º da Lei n.º 47/2006,
de 28 de agosto, podem submeter manuais escolares à avaliação para a atribuição
de certificação.

Assim, qualquer das entidades acima referidas, se produzirem um manual
em acesso aberto, poderão submetê-lo para aprovação e certificação.

A [Ref. 8] é o Decreto-Lei que define, entre outros factores, o regime de
avaliação, certificação e adopção dos manuais escolares do ensino básico e do
ensino secundário.

O artigo 8º diz:

``2 — Os procedimentos de adopção, avaliação e certificação desenvolvem-se em duas fases:

a) Uma fase de avaliação e de certificação dos manuais escolares, a cargo de comissões de 
avaliação, que se traduz na atribuição de uma certificação de qualidade científico-pedagógica;

b) Uma fase de avaliação e adopção, a realizar pelos docentes nas escolas, tendo
em vista a apreciação da adequação dos manuais certificados ao projecto educativo
respectivo.''

No meu entendimento pessoal, não parece haver na lei, nada que contradiga o
procedimento descrito na secção *3 Motivações e Roteiro* para a produção de
manuais escolares em acesso aberto.

## 6 Custos associados

Foram identificados os seguintes custos associados.

* Custo directo: avaliação e certificação dos manuais escolares por Comissões de
Avaliação (ver Artigo 9º do Decreto-Lei [Ref. 8]).
* Custo indirecto: plataforma digital (site Web) providenciada e gerida pelo
Ministério da Educação ou outra entidade certificada para o efeito, que
disponibiliza os manuais escolares certificados de forma pública.

## 7 Revisão pelos pares (Peer review)

Num sistema aberto descrito nas secções anterior, será fácil não só
contribuir para a melhoria do documento/manual, como permitir a revisão
e discussão de possíveis alterações.

A formação de grupos de trabalho e discussão de uma dada área, podem ser 
realizadas de forma a melhorar o manual.

Mais, as correções ao texto podem ser implementadas de uma forma muito rápida
e disponibilizada aos interessados, ajudada pela implementação de um sistema
de versões, e eventualmente da respectiva data.

A aprovação de alterações ficará sempre a cargo dos autores e de acordo (ou não
violando) o disposto descrito na secção *1.4 Certificação, avaliação, acreditação*.

## 8 Reconhecimento (Ackowlegments)

Os autores são reconhecidos pela produção do manual, por exemplo: existindo 
uma forma de saber quantas escolas adoptam um dado manual,
a qualidade do manual pode ser uma consequência desse reconhecimento.

Deixarei em aberto a questão de, se este pode ser um ponto que faça parte da
avaliação dos respectivos autores.

## 9 Considerações

Esta proposta tem inspiração em várias áreas, tais como:

Investigação científica: desde há alguns anos que na Europa (e não só), existe uma
motivação crescente para **a disponibilização dos resultados da investigação 
científica através da Internet, de forma aberta, livre e sem custos para o 
utilizador.** [Ref. 4, 5].

A questão põe-se, porquê apenas na investigação científica, poderá ser
naturalmente alargada ao ensino.

No contexto do *Software aberto* (Open source), em que o código de
software é disponibilizado de forma aberta, gratuita, motivando a sua
melhoria através de contribuição livre, de forma rápida, grande escrutínio
sobre a sua qualidade, descoberta de falhas e a sua rápida resolução.

Esta proposta tem em conta o facto de que os manuais escolares mudarem
com uma frequência demasiado elevada, de ano lectivo para ano lectivo,
mesmo dentro da mesma escola ou agrupamento escolar e, mesmo quando
**não existe** mudança dos programas das disciplinas.

Estas mudanças de manuais, não são claras para alunos e pais:
* Quais são os critérios para tal mudança?
* Porquê um dado manual muda no ano seguinte, quais os erros ou
matéria que motivaram essa mudança?

## 10 Contribuições

Aceitam-se contribuições para esta proposta na forma de:

* Comentários
* Modificações e texto adicional

**Não se aceitam** contribuições de forma anónima.

Podem ser usados vários métodos para contribuições:

* Criar *issue* no repositório do gitlab:
  * https://gitlab.com/mariojmdavid/manual-escolar-acesso-aberto/issues
  * É necessário ter conta, ou criá-la.
  * Pode usar a conta google ou twitter para fazer o registo.
* Como comentários no documento google:
  * https://bit.ly/me_acesso_aberto
* Envio de mail para o autor:
  * O autor responsabiliza-se a criar um issue no gitlab.
  * A discussão pode prosseguir por mail, mas ficará registada.

## 11 Código de conduta

1. **É permitida** e **incentivada** a discussão, correcção, comentários,
adição ao texto da proposta de forma construtiva.
2. As discussões devem decorrer de forma correta, civilizada e com o respeito
por todos participantes.
3. **É incentivado** o desacordo com a proposta ou pontos da proposta, desde
que devidamente fundamentados, se possível sugerindo alternativas.
4. **Não são permitidos** comentários pessoais, *insultos*, *gritos*.
5. **Não são permitidas** contribuições, comentários, etc. que sejam feitos de 
forma **anónima**.

## 12 Referências

1. Sobre licenças *Creative Commons*:
https://creativecommons.org/
2. Fundação para a Ciência e Tecnologia, Política de Acesso Aberto:
https://www.fct.pt/acessoaberto/index.phtml.pt
3. Commonwealth, Learning for Sustainable Development:
https://www.col.org/
4. Acesso Aberto (Open Access):
https://en.wikipedia.org/wiki/Open_access
5. FOSTER, Contexto da Política de Acesso Aberto do H2020:
https://www.fosteropenscience.eu/content/contexto-da-politica-de-acesso-aberto-do-h2020
6. Budapest Open Access Initiative:
https://www.budapestopenaccessinitiative.org/read
7. Direção-Geral da Educação, Avaliação e Certificação de Manuais Escolares 2017-2018:
http://www.dge.mec.pt/ano-letivo-de-2017-2018
8. Decreto-Lei n.o 47/2006:
http://www.dge.mec.pt/sites/default/files/ManuaisEscolares/2006_lei_47.pdf

